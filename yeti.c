#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <signal.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

//Komunikaty A -> B
#define INITIALIZE 1
#define BLOCK_MASTER 2
#define BLOCK_ROOM 3
#define BLOCK_PROJ 4
#define END_MASTER 5
#define END_ROOM 6
#define END_PROJ 7
#define FORCE_EXIT 10

//Komunikaty B -> A
#define INITIALIZE_OK 51
#define BLOCK_MASTER_OK 52
#define BLOCK_ROOM_OK 53
#define BLOCK_PROJ_OK 54
#define END_MASTER_OK 55
#define END_ROOM_OK 56
#define END_PROJ_OK 57

//Zewnetrzne komunikaty (B -> reszta procesow)
#define REQUEST_MASTER 101
#define REQUEST_PROJ 102
#define REQUEST_ROOM 103
#define ALLOW_MASTER 104
#define ALLOW_ROOM 105
#define ALLOW_PROJ 106
#define RELEASE_MASTER 107
#define RELEASE_PROJ 108
#define RELEASE_ROOM 109

#define INIT_VALUE 1

#define false 0
#define true 1

#define COMM_DATA_SIZE 3

#define YETI_NUMBER 5
#define PROJECTOR_NUMBER 4
#define ROOM_NUMBER 3

#define START_FORCE 4

#define LECTURE_UTIME 20
#define MEDITATION_UTIME 200

//#define DEBUG_MODE

//#define printf(...) ;

int rank, size;

int comm(int id){
    return id * 2 + 1;
}

int find_earliest_request(int *tab, int tsize){
    int i = 0;
    int id = -1;
    int min = -1;
    for (i = 0; i < tsize; i++){
	if ((min == -1 || tab[i] < min) && tab[i] != -1){
	    id = i;
	    min = tab[i];
	}
    }
    return id;
}

int count_section_permission(int *tab, int tsize, int id){
    int i = 0;
    int count = 0;
    for (i = 0; i < tsize; i++){
	if (i < id){
	    if (tab[i] <= tab[id] && tab[i] != -1){
		count++;
	    }
	}else if (i > id){
	    if (tab[i] < tab[id] && tab[i] != -1){
		count++;
	    }
	}
    }
    return count;
}

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);

    int data[COMM_DATA_SIZE];
    int swap_data[COMM_DATA_SIZE];
    
    int finished = 0;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size%2 != 0){
	MPI_Finalize();
	return -1;
    }

    srand(rank+gettimeofday(0,0));

    //int timestamp = 0;
    
    MPI_Status status;
    
    if (rank%2==0) { //Proces operacyjno taktyczny typu A
	MPI_Send(data, COMM_DATA_SIZE, MPI_INT, rank + 1, INITIALIZE, MPI_COMM_WORLD);
	MPI_Recv(data, COMM_DATA_SIZE, MPI_INT, rank + 1, INITIALIZE_OK, MPI_COMM_WORLD, &status);
	
	while (true){
	    printf("Tick %d\n", rank/2);
	  
	    int master_id = rand() % YETI_NUMBER;
	    int room_id = rand() % ROOM_NUMBER;
	    
#ifdef DEBUG_MODE
	    printf("*************Req at %d   [%d]\n", rank / 2, master_id);
#endif
	    data[0] = master_id;
	    MPI_Send(data, COMM_DATA_SIZE, MPI_INT, rank + 1, BLOCK_MASTER, MPI_COMM_WORLD);
	    MPI_Recv(data, COMM_DATA_SIZE, MPI_INT, rank + 1, BLOCK_MASTER_OK, MPI_COMM_WORLD, &status);
	    int force = data[2];
	    
	    data[0] = room_id;
	    MPI_Send(data, COMM_DATA_SIZE, MPI_INT, rank + 1, BLOCK_ROOM, MPI_COMM_WORLD);
	    MPI_Recv(data, COMM_DATA_SIZE, MPI_INT, rank + 1, BLOCK_ROOM_OK, MPI_COMM_WORLD, &status);
	    
	    
	    MPI_Send(data, COMM_DATA_SIZE, MPI_INT, rank + 1, BLOCK_PROJ, MPI_COMM_WORLD);
	    MPI_Recv(data, COMM_DATA_SIZE, MPI_INT, rank + 1, BLOCK_PROJ_OK, MPI_COMM_WORLD, &status);
	    
#ifdef DEBUG_MODE
	    printf("====Wyklad (mistrz %d pokoj %d proc %d) force: %d\n", master_id, room_id, rank / 2, force);
#endif
	    usleep(LECTURE_UTIME);
	    
	    MPI_Send(data, COMM_DATA_SIZE, MPI_INT, rank + 1, END_PROJ, MPI_COMM_WORLD);
	    MPI_Recv(data, COMM_DATA_SIZE, MPI_INT, rank + 1, END_PROJ_OK, MPI_COMM_WORLD, &status);
	    
	    MPI_Send(data, COMM_DATA_SIZE, MPI_INT, rank + 1, END_ROOM, MPI_COMM_WORLD);
	    MPI_Recv(data, COMM_DATA_SIZE, MPI_INT, rank + 1, END_ROOM_OK, MPI_COMM_WORLD, &status);
	    
	    force--;
	    if (force <= 0){
#ifdef DEBUG_MODE
		printf(">>>>Medytacje/zbieranie mocy przy uzyciu listy obecnosci (mistrz %d proc %d)\n", master_id, rank / 2);
#endif
		usleep(MEDITATION_UTIME);
		force = START_FORCE;
	    }
#ifdef DEBUG_MODE
	    printf("*************Done at %d   [%d]\n", rank / 2, master_id);
#endif
	    data[2] = force;
	    MPI_Send(data, COMM_DATA_SIZE, MPI_INT, rank + 1, END_MASTER, MPI_COMM_WORLD);
	    MPI_Recv(data, COMM_DATA_SIZE, MPI_INT, rank + 1, END_MASTER_OK, MPI_COMM_WORLD, &status);
	}
	
	//printf("Exiting %d\n", rank / 2);
	//MPI_Send(data, COMM_DATA_SIZE, MPI_INT, rank + 1, FORCE_EXIT, MPI_COMM_WORLD);
	
    } else { //Proces komunikacyjny typu B
	int comm_loop = true;
	int sender = -1;
	int tag = -1;
	
	int i;
	
	int parent_id = rank - 1;
	int others = size / 2 - 1;
	int this_id = rank / 2;
	
	int chosen_master = -1;
	int chosen_room = -1;
	int chosen_proj = false;
	
	int last_timestamp = 0;
	
	MPI_Recv(data, COMM_DATA_SIZE, MPI_INT, parent_id, INITIALIZE, MPI_COMM_WORLD, &status);
	MPI_Send(data, COMM_DATA_SIZE, MPI_INT, parent_id, INITIALIZE_OK, MPI_COMM_WORLD);
	
	int processing_master = false;
	int master_permissions = 0;
	
	int **waiting_master = (int**) malloc(sizeof(int*) * YETI_NUMBER);
	for (i = 0; i < YETI_NUMBER; i++){
	    waiting_master[i] = (int*) malloc(sizeof(int) * size / 2);
	    memset(waiting_master[i], -1, size / 2 * sizeof(int));
	}
	int *force_master = (int*) malloc(sizeof(int) * YETI_NUMBER);
	for (i = 0; i < YETI_NUMBER; i++){
	    force_master[i] = START_FORCE;
	}
	
	int *time_master = (int*) malloc(sizeof(int) * YETI_NUMBER);
	for (i = 0; i < YETI_NUMBER; i++){
	    time_master[i] = 0;
	}
	
	int processing_room = false;
	int room_permissions = 0;
	
	int **waiting_room = (int**) malloc(sizeof(int*) * ROOM_NUMBER);
	for (i = 0; i < ROOM_NUMBER; i++){
	    waiting_room[i] = (int*) malloc(sizeof(int) * size / 2);
	    memset(waiting_room[i], -1, size / 2 * sizeof(int));
	}
	
	int processing_proj = false;
	int proj_permissions = 0;
	
	int *waiting_proj = (int*) malloc(sizeof(int) * size / 2);
	for (i = 0; i < size / 2; i++){
	    waiting_proj[i] = -1;
	}

	while(comm_loop){///////////////////////////////////

	    MPI_Recv(data, COMM_DATA_SIZE, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	    sender = status.MPI_SOURCE;
	    tag = status.MPI_TAG; 
	    
	    int sender_id = sender / 2;
	    
	    last_timestamp = MAX(data[1] + 1, last_timestamp + 1);
	    swap_data[1] = last_timestamp;
	    
	    switch (tag){
		case BLOCK_MASTER:
		    chosen_master = data[0];
		    swap_data[0] = chosen_master;
		    master_permissions = 0;
		    waiting_master[chosen_master][this_id] = last_timestamp;
		    
		    for (i = 0; i < size / 2; i++){
			int recv_id = comm(i);
			if (recv_id != rank){
			    MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, recv_id, REQUEST_MASTER, MPI_COMM_WORLD);
			}
		    }
		    break;
		    
		case END_MASTER:
		    swap_data[0] = chosen_master;
		    if (chosen_master != -1){
			force_master[chosen_master] = data[2];
			time_master[chosen_master] = last_timestamp;
			swap_data[2] = data[2];
			for (i = 0; i < size / 2; i++){
			    if (i != this_id){
				MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, comm(i), RELEASE_MASTER, MPI_COMM_WORLD);
			    }
			}
			waiting_master[chosen_master][this_id] = -1;
			
		    }
		    chosen_master = -1;
		    processing_master = false;
		    
		    MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, parent_id, END_MASTER_OK, MPI_COMM_WORLD);
		    break;
		    
		case REQUEST_MASTER:
		    swap_data[0] = chosen_master;
		    if (chosen_master != -1){
			swap_data[1] = waiting_master[chosen_master][this_id];
		    }else{
			swap_data[1] = -1;
		    }
		    
		    if (data[0] != -1){
			waiting_master[data[0]][sender_id] = data[1];
		    }
		    
		    MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, sender, ALLOW_MASTER, MPI_COMM_WORLD);
		    break;
		    
		case ALLOW_MASTER:
		    if (data[0] != -1){
			waiting_master[data[0]][sender_id] = data[1];
		    }
		    master_permissions++;
		    if (master_permissions == others){
			if (chosen_master != -1){
			    int find_id = find_earliest_request(waiting_master[chosen_master], size/2);
			    if (find_id == this_id && !processing_master){
				data[2] = force_master[chosen_master];
				processing_master = true;
#ifdef DEBUG_MODE
				printf("----Got master %d   at (%d <- empty) lastp: (%d: %d <%d>)...\n", chosen_master, this_id, sender_id, data[0], data[1]);
#endif
				MPI_Send(data, COMM_DATA_SIZE, MPI_INT, parent_id, BLOCK_MASTER_OK, MPI_COMM_WORLD);
			    }
			}
		    }
		    break;
		    
		case RELEASE_MASTER:
		    if (data[0] != -1){
			    waiting_master[data[0]][sender_id] = -1;
			    if (data[1] > time_master[data[0]]){
				time_master[data[0]] = data[1];
				force_master[data[0]] = data[2];
			    }
		    }
		    if (data[0] == chosen_master){
			if (chosen_master != -1){
			    int find_id = find_earliest_request(waiting_master[chosen_master], size/2);
			    if (find_id == this_id && !processing_master){
				data[2] = force_master[chosen_master];
				processing_master = true;
#ifdef DEBUG_MODE
				printf("----Got master %d   at (%d <- %d)...\n", data[0], this_id, sender_id);
#endif
				MPI_Send(data, COMM_DATA_SIZE, MPI_INT, parent_id, BLOCK_MASTER_OK, MPI_COMM_WORLD);
			    }
			}
		    }
		    break;
		    
		//////////////////////////////////////////////////////////////////////////
		case BLOCK_ROOM:
		    chosen_room = data[0];
		    swap_data[0] = chosen_room;
		    room_permissions = 0;
		    waiting_room[chosen_room][this_id] = last_timestamp;
		    
		    for (i = 0; i < size / 2; i++){
			int recv_id = comm(i);
			if (recv_id != rank){
			    MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, recv_id, REQUEST_ROOM, MPI_COMM_WORLD);
			}
		    }
		    break;
		    
		case END_ROOM:
		    swap_data[0] = chosen_room;
		    if (chosen_room != -1){
			for (i = 0; i < size / 2; i++){
			    if (i != this_id){
				MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, comm(i), RELEASE_ROOM, MPI_COMM_WORLD);
			    }
			}
			waiting_room[chosen_room][this_id] = -1;
			
		    }
		    chosen_room = -1;
		    processing_room = false;
		    
		    MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, parent_id, END_ROOM_OK, MPI_COMM_WORLD);
		    break;
		    
		case REQUEST_ROOM:
		    swap_data[0] = chosen_room;
		    if (chosen_room != -1){
			swap_data[1] = waiting_room[chosen_room][this_id];
		    }else{
			swap_data[1] = -1;
		    }
		    
		    if (data[0] != -1){
			waiting_room[data[0]][sender_id] = data[1];
		    }
		    
		    MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, sender, ALLOW_ROOM, MPI_COMM_WORLD);
		    break;
		    
		case ALLOW_ROOM:
		    if (data[0] != -1){
			waiting_room[data[0]][sender_id] = data[1];
		    }
		    room_permissions++;
		    if (room_permissions == others){
			if (chosen_room != -1){
			    int find_id = find_earliest_request(waiting_room[chosen_room], size/2);
			    if (find_id == this_id && !processing_room){
				processing_room = true;
#ifdef DEBUG_MODE
				printf("----Got room %d   at (%d <- empty) lastp: (%d: %d <%d>)...\n", chosen_room, this_id, sender_id, data[0], data[1]);
#endif
				MPI_Send(data, COMM_DATA_SIZE, MPI_INT, parent_id, BLOCK_ROOM_OK, MPI_COMM_WORLD);
			    }
			}
		    }
		    break;
		    
		case RELEASE_ROOM:
		    if (data[0] != -1){
			    waiting_room[data[0]][sender_id] = -1;
		    }
		    if (data[0] == chosen_room){
			if (chosen_room != -1){
			    int find_id = find_earliest_request(waiting_room[chosen_room], size/2);
			    if (find_id == this_id && !processing_room){
				processing_room = true;
#ifdef DEBUG_MODE
				printf("----Got room %d   at (%d <- %d)...\n", data[0], this_id, sender_id);
#endif
				MPI_Send(data, COMM_DATA_SIZE, MPI_INT, parent_id, BLOCK_ROOM_OK, MPI_COMM_WORLD);
			    }
			}
		    }
		    break;
		    
		/////////////////////////////////////////////////////////////////////    
		case BLOCK_PROJ:
		    chosen_proj = true;
		    proj_permissions = 0;
		    waiting_proj[this_id] = last_timestamp;
		    
		    swap_data[0] = chosen_proj;
		    for (i = 0; i < size / 2; i++){
			int recv_id = comm(i);
			if (recv_id != rank){
			    MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, recv_id, REQUEST_PROJ, MPI_COMM_WORLD);
			}
		    }
		    break;
		    
		case END_PROJ:
		    swap_data[0] = chosen_proj;
		    if (chosen_proj != false){
			for (i = 0; i < size / 2; i++){
			    if (i != this_id){
				MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, comm(i), RELEASE_PROJ, MPI_COMM_WORLD);
			    }
			}
			waiting_proj[this_id] = -1;
			
		    }
		    chosen_proj = false;
		    processing_proj = false;
		    
		    MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, parent_id, END_PROJ_OK, MPI_COMM_WORLD);
		    break;
		    
		case REQUEST_PROJ:
		    swap_data[0] = chosen_proj;
		    if (chosen_proj != false){
			swap_data[1] = waiting_proj[this_id];
		    }else{
			swap_data[1] = -1;
		    }
		    
		    if (data[0] != false){
			waiting_proj[sender_id] = data[1];
		    }
		    
		    MPI_Send(swap_data, COMM_DATA_SIZE, MPI_INT, sender, ALLOW_PROJ, MPI_COMM_WORLD);
		    break;
		    
		case ALLOW_PROJ:
		    if (data[0] != false){
			waiting_proj[sender_id] = data[1];
		    }
		    proj_permissions++;
		    if (proj_permissions == others){
			if (chosen_proj != false){
			    //int find_id = find_earliest_request(waiting_room[chosen_room], size/2);
			    int taken = count_section_permission(waiting_proj, size/2, this_id);
			    if (taken < PROJECTOR_NUMBER && !processing_proj){
				processing_proj = true;
#ifdef DEBUG_MODE
				printf("----Got proj at (%d <- empty) lastp: (%d: <%d>)...\n", this_id, sender_id, data[1]);
#endif
				MPI_Send(data, COMM_DATA_SIZE, MPI_INT, parent_id, BLOCK_PROJ_OK, MPI_COMM_WORLD);
			    }
			}
		    }
		    break;
		    
		case RELEASE_PROJ:
		    if (data[0] == true){
			waiting_proj[sender_id] = -1;
			if (chosen_proj != false){
			    int taken = count_section_permission(waiting_proj, size/2, this_id);
			    if (taken < PROJECTOR_NUMBER && !processing_proj){
				processing_proj = true;
#ifdef DEBUG_MODE
				printf("----Got proj at (%d <- %d)...\n", this_id, sender_id);
#endif
				MPI_Send(data, COMM_DATA_SIZE, MPI_INT, parent_id, BLOCK_PROJ_OK, MPI_COMM_WORLD);
			    }
			}
		    }
		    break;
		    
		///////////////////////////////////////////////////////////////////// 
		case FORCE_EXIT:
		    comm_loop = false;
		    break;
		    
		default:
		    printf("Unknown message: %d from %d\n", tag, sender);
		    break;
		    
	    }
	    
	}//////////////////////////////////////////////////
	
	free(waiting_proj);
	
	for (i = 0; i < ROOM_NUMBER; i++){
	    free(waiting_room[i]);
	}
	free(waiting_room);
	
	free(time_master);
	free(force_master);
	
	for (i = 0; i < YETI_NUMBER; i++){
	    free(waiting_master[i]);
	}
	free(waiting_master);
    }

    MPI_Finalize();
    return 0;
}